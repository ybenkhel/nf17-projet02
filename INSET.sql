INSERT INTO animal Values (1,NULL,'MAX',23,'calme',TO_DATE('20170515','YYYYMMDD'),NULL,0645020701,3)
INSERT INTO animal Values (2,NULL,'toutou',23,'calme',TO_DATE('20150805','YYYYMMDD'),NULL,0644024401,2)
INSERT INTO animal Values (3,0987,'bella',23,'calme',TO_DATE('20140710','YYYYMMDD'),NULL,0646520301,4)
INSERT INTO animal Values (4,NULL,'bille',23,'calme',TO_DATE('20170425','YYYYMMDD'),NULL,0646060101,1)

INSERT INTO RDV Values (1,1,3,1,NULL)
INSERT INTO RDV Values (2,2,2,2,NULL)
INSERT INTO RDV Values (3,3,1,1,NULL)
INSERT INTO RDV Values (4,4,4,2,NULL)

INSERT INTO consultation Values(1,1)
INSERT INTO consultation Values(2,4)

INSERT INTO Intervention Values(1,2)
INSERT INTO Intervention Values(2,3)

INSERT INTO Ordonnance Values(1,'a tenir loin des enfants',4,2)
INSERT INTO Ordonnance Values(2,'a tenir loin des enfants et les femmes enceintes',1,3)
INSERT INTO Ordonnance Values(3,'a tenir loin des enfants',2,4)
INSERT INTO Ordonnance Values(4,'a tenir loin des enfants. A prendre apres un repas',2,1)


INSERT INTO Facture Values(1,TO_DATE('20170516','YYYYMMDD'),cash,1,2)
INSERT INTO Facture Values(2,TO_DATE('20170119','YYYYMMDD'),carte bancaire,2,4)
INSERT INTO Facture Values(3,TO_DATE('20180412','YYYYMMDD'),cash,3,2)
INSERT INTO Facture Values(4,TO_DATE('20180516','YYYYMMDD'),cash,4,1)



INSERT INTO Medicament Values('Duphalac',16,30)
INSERT INTO Medicament Values('Nifluril',10,35)
INSERT INTO Medicament Values('Flunir',1,10)
INSERT INTO Medicament Values('Désomédine',16,25)
INSERT INTO Medicament Values('Hexaseptine',6,40)


INSERT INTO Employe Values(1,'yasine','trabelsi')
INSERT INTO Employe Values(2,'Louise','pau')
INSERT INTO Employe Values(3,'asta','yuno')


INSERT INTO Proprietaire Values (0645020701,'jeanne','Dubois')
INSERT INTO Proprietaire Values (0644024401,NULL,NULL)
INSERT INTO Proprietaire Values (0646520301,'jean','dupont')
INSERT INTO Proprietaire Values (0646060101,'Emily','Ben')


INSERT INTO Veterinaire Values(1,'Charles','Benjamin')
INSERT INTO Veterinaire Values(2,'jean','paul')
INSERT INTO Veterinaire Values(3,'Rachid','ben')
INSERT INTO Veterinaire Values(4,'youssef','something')

INSERT INTO Race Values(1,20)
INSERT INTO Race Values(2,30)
INSERT INTO Race Values(3,45)
INSERT INTO Race Values(4,15)

INSERT INTO Espece Values(1,20,1)
INSERT INTO Espece Values(2,25,1)
INSERT INTO Espece Values(3,55,1)
INSERT INTO Espece Values(4,30,3)

INSERT INTO Gerer Values(1,1)
INSERT INTO Gerer Values(2,1)
INSERT INTO Gerer Values(3,2)
INSERT INTO Gerer Values(4,3)


INSERT INTO RefMed Values('Duphalac',1)
INSERT INTO RefMed Values('Nifluril',1)
INSERT INTO RefMed Values('Flunir',2)
INSERT INTO RefMed Values('Hexaseptine',3)
INSERT INTO RefMed Values('Hexaseptine',4)
INSERT INTO RefMed Values('Désomédine',4)
