CREATE TABLE Animal (
  CodeAnimal int UNIQUE NOT NULL,
  CodeID:int UNIQUE,
  Nom:VARCHAR(25) NOT NULL ,
  poids:int NOT NULL ,
  genre:VARCHAR(255) NOT NULL,
  DdN:Date NOT NULL ,
  Taille:int,
  numeroTel int NOT NULL,
  IDespece int,
  PRIMARY KEY (CodeAnimal),
  FOREIGN KEY (numeroTel) REFERENCES Proprietaire(numeroTel),
  FOREIGN KEY (IDespece) REFERENCES Espece(IDespece)
)
--animal(#CodeAnimal:int,CodeID:int,Nom:text,poids:int,genre:text,DdN:Date,Taille:int,numeroTel=>Proprietaire,IDespece=>Espece) --Taille NULLABLE
CREATE TABLE RDV (
  codeRDV int UNIQUE NOT NULL,
  CodeAnimal int NOT NULL ,
  IDvet int NOT NULL,
  IDcon int ,
  IDInter int ,
  PRIMARY KEY (CodeAnimal),
  FOREIGN KEY (CodeAnimal) REFERENCES Animal(CodeAnimal),
  FOREIGN KEY (IDvet) REFERENCES Veterinaire(IDvet),
  FOREIGN KEY (IDcon) REFERENCES consultation(IDcon),
  FOREIGN KEY (IDInter) references Intervention(IDInter)
)
--RDV(#codeRDV,CodeAnimal=>animal.CodeAnimal,IDvet=>Veterinaire.IDvet,IDcon=>consultation.IDcon,IDInter=>Intervention.IDInter) -- IDcon KEY - IDInter KEY
CREATE TABLE consultation(
  IDcon int UNIQUE NOT NULL,
  IDFactu int,
  PRIMARY KEY (IDcon),
  FOREIGN KEY (IDFactu) REFERENCES Facture(IDFactu)
)
--consultation(#IDcons:int,prix:int,IDFactu=>Facture.IDFactu)
CREATE TABLE  Intervention(
  IDInter int UNIQUE NOT NULL,
  IDFactu int ,
  PRIMARY KEY (IDInter),
  FOREIGN KEY (IDFactu) REFERENCES Facture(IDFactu)
)
--Intervention(#IDInter:int,prix:int,IDFactu=>Facture.IDFactu)
CREATE TABLE Ordonnance(
  IDOrd int UNIQUE NOT NULL,
  instruction VARCHAR(225) NOT NULL,
  CodeAnimal int NOT NULL,
  IDvet int NOT NULL ,
  PRIMARY KEY(IDOrd),
  FOREIGN KEY (CodeAnimal) REFERENCES Animal(CodeAnimal),
  FOREIGN KEY (IDvet) REFERENCES Veterinaire(IDvet)
)
--Ordonnance(#IDOrd,Nombre:int,instruction:text,CodeAnimal=>animal.CodeAnimal,IDvet=>Veterinaire.IDvet)
CREATE TABLE Facture(
  IDFactu int UNIQUE NOT NULL,
  dateF date,
  Moyendepaiment VARCHAR(25),
  CodeAnimal int NOT NULL ,
  IDOrd int,
  PRIMARY KEY(IDFactu),
  FOREIGN KEY(CodeAnimal) REFERENCES Animal(CodeAnimal),
  FOREIGN KEY(IDOrd) REFERENCES Ordonnance(IDOrd)
)
--Facture(#IDFactu,montant:int,dateF:date,Moyen de paiment:Text,CodeAnimal=>animal.CodeAnimal,IDOrd=>Ordonnance.IDOrd)-- Montant NULLABLE - dateF NULLABLE - Moyen de paiment NULLABLE
CREATE TABLE Medicament(
  nomMedicament VARCHAR(25) UNIQUE NOT NULL,
  quantite int NOT NULL,
  prix: int NOT NULL,
  PRIMARY KEY(nomMedicament)
)
--Medicament(#nomMedicament:text,quantite:int,prix:int)
CREATE TABLE Employe(
  IDEmploye int UNIQUE NOT NULL,
  nom VARCHAR(25) NOT NULL,
  prenom VARCHAR(25) NOT NULL,
  PRIMARY KEY(IDEmploye)
)
--Employe(#IDEmploye:int,nom:text,prenom:text)
CREATE TABLE Proprietaire(
  numeroTel int UNIQUE NOT NULL,
  nom VARCHAR(25) NOT NULL,
  prenom VARCHAR(25) NOT NULL,
  PRIMARY KEY(numeroTel)
)
--Proprietaire(#numeroTel:int,nom:text,prenom:text)
CREATE TABLE Veterinaire(
  IDvet int UNIQUE NOT NULL,
  nom VARCHAR(25),
  prenom VARCHAR(25),
  PRIMARY KEY(IDvet)
)
--Veterinaire(#IDvet:int,nom:text,prenom:text)
CREATE TABLE Race(
  IDRace int UNIQUE NOT NULL,
  prix int NOT NULL,
  PRIMARY KEY(IDRace)
)

--Race(#IDRace:int,prix:entier)
CREATE TABLE Espece(
  IDespece int UNIQUE NOT NULL,
  prix int NOT NULL,
  IDRace int NOT NULL,
  PRIMARY KEY(IDespece),
  FOREIGN KEY(IDRace) REFERENCES Race(IDRace)
)
--Espece(#IDespece:int,Prix:entier,IDRace=>Race.IDRace)
CREATE TABLE Gerer(
  IDFactu int NOT NULL,
  IDEmploye int NOT NULL,
  CONSTRAINT PK_Gerer PRIMARY KEY (IDFactu, IDEmploye),
  FOREIGN KEY(IDFactu) REFERENCES Facture(IDFactu),
  FOREIGN KEY(IDEmploye) REFERENCES Employe(IDEmploye)

)
--Gerer(#IDFactu=>Facture.IDFactu,#IDEmploye=>Employe.IDEmploye)
CREATE TABLE RefMed(
  nomMedicament VARCHAR(25) NOT NULL,
  IDOrd int NOT NULL,
  CONSTRAINT PK_RefMed PRIMARY KEY (nomMedicament, IDOrd),
  FOREIGN KEY(nomMedicament) REFERENCES Medicament(nomMedicament),
  FOREIGN KEY(IDOrd) REFERENCES Ordonnance(IDOrd)

)
--RefMed(#nomMedicament=>Medicament.nomMedicament,#IDOrd=>Ordonnance.IDOrd)
/*
contraintes:
contraintes:
PROJECTION(Facture,IDFactu)= PROJECTION(Gerer,IDFactu) AND PROJECTION(Employe,#IDEmploye) = PROJECTION(Gerer,#IDEmploye)
PROJECTION(Medicament,nomMedicament)=PROJECTION(RefMed,nomMedicament)
animal.numeroTel NOT NULL and PROJECTION(animal,numeroTel)=PROJECTION(animal,Proprietaire)
PROJECTION(Espece,IDespece)=PROJECTION(animal,IDespece)
PROJECTION(Espece,IDRace)=PROJECTION(Race,IDRace)
RDV.CodeAnimal NOT NULL and PROJECTION(animal,CodeAnimal)=PROJECTION(RDV,CodeAnimal)
Facture.CodeAnimal NOT NULL and PROJECTION(animal,CodeAnimal)=PROJECTION(Facture,CodeAnimal)
Ordonnance.CodeAnimal NOT NULL
PROJECTION(consultation,IDFactu)=PROJECTION(Facture,IDFactu)
PROJECTION(Intervention,IDFactu)=PROJECTION(Facture,IDFactu)
PROJECTION(Ordonnance,IDOrd)=PROJECTION(Facture,IDOrd)
Ordonnance.IDvet NOT NULL
RDV.IDvet NOT NULL

PROJECTION(Animal,CodeAnimal)=PROJECTION(RDV,CodeAnimal)=PROJECTION(Facture,CodeAnimal)=PROJECTION(Ordonnance,CodeAnimal)
*/
